/*
 * DicomEdit: InlineCommentRemover
 * XNAT http://www.xnat.org
 * Copyright (c) 2024, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.dicom.dicomedit;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.commons.lang3.StringUtils;
import org.nrg.dicom.mizer.exceptions.MizerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * InlineCommentRemover strips in-line comments from lines.
 * An in-line comment begins with '//' and causes the line to be truncated at this token.
 *
 * Use the ANTLR grammar in Comment.g4 to locate comments. Locating comments is complicated by
 * 1. lines containing URLs (which contain '//')
 * 2. and strings, text between '"', that contain '//' but do not constitute in-line comments.
 */
public class InlineCommentRemover extends CommentBaseVisitor<Void> {
    private static final Logger logger = LoggerFactory.getLogger(InlineCommentRemover.class);

    ParseTree _parseTree;
    List<String> linesOut = new ArrayList<>();

    /**
     * removeInLineComments
     *
     * @param linesIn list of lines to be processed.
     * @return list of lines, edited as needed.
     * @throws MizerException when linesIn fail to parse.
     */
    public List<String> removeInLineComments(List<String> linesIn) throws MizerException {
        linesOut = new ArrayList<>();
        _parseTree = createParseTree(linesIn);
        this.visit(_parseTree);
        return linesOut;
    }

    /**
     * visitLine: process a line.
     * If the line contains one or more comment tokens,
     * find the first comment token, remove it and all content to the end,
     * terminate the line with \n.
     *
     * @param lineCtx the parse tree
     * @return Void
     */
    @Override
    public Void visitLine(CommentParser.LineContext lineCtx) {
        if (!lineCtx.comment().isEmpty()) {
            CommentParser.CommentContext commentCtx = lineCtx.comment(0);
            String line = lineCtx.getText();
            String editedLine = line.substring(0,commentCtx.start.getCharPositionInLine()) + "\n";
            linesOut.add(editedLine);
        } else {
            linesOut.add(lineCtx.getText());
        }
        return null;
    }

    /**
     * createParseTree
     *
     * @param lines list of lines to be parsed
     * @return the ANTLR parse tree
     * @throws MizerException when lines fail to parse.
     */
    private ParseTree createParseTree(List<String> lines) throws MizerException {
        logger.debug("Parsing lines for Comment syntax.");
        try (final InputStream input = getInputStream(lines)) {
            return new CommentParser(new CommonTokenStream(new CommentLexer(new ANTLRInputStream(input)))) {{
                addErrorListener(new BaseErrorListener() {
                    @Override
                    public void syntaxError(final Recognizer<?, ?> recognizer,
                                            final Object offendingSymbol,
                                            final int line,
                                            final int position,
                                            final String message,
                                            final RecognitionException e) {
                        final String errorMessage = MessageFormat.format("Failed to parse at {0}:{1} due to {2}", line, position, message);
                        logger.error(errorMessage, e);
                        throw new IllegalStateException(errorMessage, e);
                    }
                });
            }}.lines();
        } catch (Exception e) {
            throw new MizerException("CommentParser failed.", e);
        }
    }

    /**
     * getInputStream
     *
     * @param lines list of lines. lines must terminate with newline.
     * @return inputStream to lines as a list of bytes.
     */
    private InputStream getInputStream(List<String> lines) {
        return new ByteArrayInputStream(
            lines.stream()
                .map(this::mustEndWithNewline)
                .collect(Collectors.joining(""))
                .getBytes()
         );
    }

    private String mustEndWithNewline(String line) {
        return line == null ? "\n" : StringUtils.appendIfMissing(line, "\n");
    }
}
