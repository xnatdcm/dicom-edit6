package org.nrg.dicom.dicomedit.pixeledit.validation;

public class ImageProcessingException extends Exception {
    public ImageProcessingException(String msg) {
        super(msg);
    }
}
